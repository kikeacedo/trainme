/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.callbacks;

/*
 * @author Quique Acedo 
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface UserTypeCallback {
    /**
     * Metodo que se ejecutara cuando el usuario logeado sea un trainer
     * @param username
     */
    void onTrainerLogged(String username);

    /**
     * Metodo que se ejecutara cuando el usuario logeado sea un trainee
     * @param username
     */
    void onTraineeLogged(String username);
}//class
