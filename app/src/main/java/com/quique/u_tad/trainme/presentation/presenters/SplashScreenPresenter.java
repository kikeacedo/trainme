/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.presenters;

import android.content.Context;

import com.quique.u_tad.trainme.domain.interactors.UserInteractor;
import com.quique.u_tad.trainme.domain.interfaces.UserInteractorInterface;
import com.quique.u_tad.trainme.presentation.activities.ContactsActivity;
import com.quique.u_tad.trainme.presentation.activities.LoginActivity;
import com.quique.u_tad.trainme.presentation.activities.WorkoutsActivity;
import com.quique.u_tad.trainme.presentation.callbacks.LaunchActivityCallback;
import com.quique.u_tad.trainme.presentation.interfaces.SplashScreen;

/*
 * @author Quique Acedo 
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class SplashScreenPresenter implements SplashScreen {

    private UserInteractorInterface userInteractor;

    public SplashScreenPresenter(Context context) {
        userInteractor = new UserInteractor();
    }

    @Override
    public void isLogged(LaunchActivityCallback callback) {
        if(userInteractor.isLogged()){
            switch (userInteractor.getUserType()){
                case TRAINEE:
                    callback.launchActivity(WorkoutsActivity.class);
                    break;
                case TRAINER:
                    callback.launchActivity(ContactsActivity.class);
                    break;
            }
        }else{
            callback.launchActivity(LoginActivity.class);
        }//if-else

    }//isLogged
}//class
