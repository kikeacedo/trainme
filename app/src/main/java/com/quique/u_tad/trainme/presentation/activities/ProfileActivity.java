/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public class ProfileActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_form, getString(R.string.nav_profile));

        TextView title = (TextView) findViewById(R.id.contact_form_title);

        title.setVisibility(View.GONE);
    }

    /**
     * Reload navigation drawer
     */
    public void reloadMenu() {

        super.reloadMenu();
    }


}//class
