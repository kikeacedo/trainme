/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.presenters;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.quique.u_tad.trainme.domain.interactors.UserInteractor;
import com.quique.u_tad.trainme.domain.interactors.WorkoutsInteractor;
import com.quique.u_tad.trainme.domain.interfaces.UserInteractorInterface;
import com.quique.u_tad.trainme.domain.interfaces.WorkoutsInteractorInterface;
import com.quique.u_tad.trainme.models.User;
import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.presentation.activities.WorkoutsActivity;
import com.quique.u_tad.trainme.presentation.callbacks.GetWorkoutCallback;
import com.quique.u_tad.trainme.presentation.callbacks.GetWorkoutsCallback;
import com.quique.u_tad.trainme.presentation.callbacks.UserTypeCallback;
import com.quique.u_tad.trainme.presentation.fragments.FormWorkoutDialogFragment;
import com.quique.u_tad.trainme.presentation.interfaces.AnimationFloatingButtonInterface;
import com.quique.u_tad.trainme.presentation.interfaces.ListInterface;
import com.quique.u_tad.trainme.presentation.interfaces.NewItemDialog;
import com.quique.u_tad.trainme.presentation.interfaces.SendTokenInterface;
import com.quique.u_tad.trainme.presentation.interfaces.UserTypeInterface;
import com.quique.u_tad.trainme.presentation.interfaces.WorkoutsInterface;
import com.quique.u_tad.trainme.utils.Constants;

import java.util.ArrayList;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo 
 * @date 5/5/16
 * @subject Programacion Plataformas Moviles
 */
public class WorkoutsPresenter implements NewItemDialog, SendTokenInterface,
        WorkoutsInterface, AnimationFloatingButtonInterface, ListInterface, UserTypeInterface {

    private WorkoutsActivity activity;
    private UserInteractorInterface loginInteractor;
    private WorkoutsInteractorInterface workoutsInteractor;
    private boolean isFabOpen = false;

    public WorkoutsPresenter() {
        workoutsInteractor = new WorkoutsInteractor();
        loginInteractor = new UserInteractor();

    }

    public WorkoutsPresenter(WorkoutsActivity activity) {
        this.activity = activity;
        loginInteractor = new UserInteractor();
        workoutsInteractor = new WorkoutsInteractor();
    }

    @Override
    public void startFormItemDialog(int id) {
        FormWorkoutDialogFragment formDialog;

        if (id >= 0) {
            formDialog = new FormWorkoutDialogFragment(id);
        } else {
            formDialog = new FormWorkoutDialogFragment();
        }

        formDialog.setCallback(new GetWorkoutCallback() {
            @Override
            public void onSucces(Workout workout, String action) {

                switch (action) {
                    case Constants.ACTION_ADD:
                        notifyItemInserted(workout);
                        break;

                    case Constants.ACTION_EDIT:
                        workoutsInteractor.updateWorkout(workout);
                        break;
                }

                getWorkoutsFromDB();
                reloadFab();
            }

            @Override
            public void onError(boolean notify) {
                if(notify){
                    Toast.makeText(activity, activity.getText(R.string.dialog_create_workout_error) + "!", Toast.LENGTH_SHORT).show();
                }
                reloadFab();
            }
        });

        formDialog.show(activity.getFragmentManager(), "FormFragment");
    }

    @Override
    public void animateFab() {
        if (isFabOpen) {
            activity.animateFab(activity.rotate_backward);
            isFabOpen = false;
        } else {
            activity.floatingActionButton.startAnimation(activity.rotate_forward);
            isFabOpen = true;
        }
    }

    @Override
    public void reloadFab() {
        if (isFabOpen) {
            activity.animateFab(activity.rotate_backward);
            isFabOpen = false;
        }
    }

    @Override
    public void sendTokenByEmail() {
        String subject = "Trainme - Registration ID";
        String body = "";

        User user = loginInteractor.getUser();

        if (user != null) {
            body += activity.getString(R.string.mail_username) + " " + user.getName() + " " + user.getSurname();
            body += activity.getString(R.string.mail_mail) + " " + user.getEmail();
            body += "-------------";
            body += user.getToken();
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        try {
            activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.send_mail)));
        } catch (ActivityNotFoundException exception) {
            Log.e("Error: ", exception.toString());
        }
    }

    @Override
    public void getWorkouts(GetWorkoutsCallback callback) {
        ArrayList<Workout> list = workoutsInteractor.getWorkouts();

        list.add(new Workout("", ""));

        if (list.size() > 0) {
            callback.onSucces(list);
        } else {
            callback.onError();
        }//if-else
    }

    @Override
    public void getWorkout(int id, GetWorkoutCallback callback) {
        Workout workout = workoutsInteractor.getWorkout(id);

        if (workout != null) {
            callback.onSucces(workout, Constants.ACTION_EDIT);
        } else {
            callback.onError(false);
        }

    }


    @Override
    public void getUserType(UserTypeCallback callback) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);

        switch (preferences.getString(Constants.PREF_USER_TYPE, "NONE")) {
            case Constants.USER_TYPE_TRAINEE:
                callback.onTraineeLogged("");
                break;

            case Constants.USER_TYPE_TRAINER:
                callback.onTrainerLogged("");
                break;

            case "NONE":
                break;
        }
    }

    @Override
    public void notifyItemRemoved(Object object) {
        final Workout workout = (Workout) object;

        workoutsInteractor.removeWorkout(workout);

        getWorkouts(new GetWorkoutsCallback() {
            @Override
            public void onSucces(ArrayList<Workout> workouts) {
                if (workouts != null) {
                    activity.reloadAdapter(workouts);
                    getUserType(new UserTypeCallback() {
                        @Override
                        public void onTrainerLogged(String username) {
                            Toast.makeText(activity, activity.getText(R.string.card_delete) + " " +workout.getTitle() + "!", Toast.LENGTH_SHORT).show();

                        }

                        @Override
                        public void onTraineeLogged(String username) {
                        }
                    });
                    Toast.makeText(activity, activity.getText(R.string.card_delete) + " " + workout.getTitle() + "!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError() {
                Toast.makeText(activity, activity.getText(R.string.card_delete_error) + " " + workout.getTitle() + "!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void notifyItemInserted(Object object) {
        final Workout workout = (Workout) object;
        workoutsInteractor.addWorkout(workout);
    }

    private void getWorkoutsFromDB() {
        ArrayList<Workout> list = workoutsInteractor.getWorkouts();

//        if(list.size() > 3) {
        list.add(new Workout("", ""));
//        }

        activity.reloadAdapter(list);
    }
}//class
