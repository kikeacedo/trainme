/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.quique.u_tad.trainme.presentation.callbacks.UserTypeCallback;
import com.quique.u_tad.trainme.presentation.presenters.MainPresenter;
import com.quique.u_tad.trainme.utils.Constants;
import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public class BaseActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String username, userType;
    private NavigationView navigationView;
    private MainPresenter presenter;

    protected Toolbar toolbar;

    private RelativeLayout fullLayout;
    private FrameLayout frameLayout;
    private String title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }//onCreate

    /**
     * Redraw navigation drawer
     */
    public void reloadMenu() {
        drawNavigationDrawer();
    }

    /**
     * Overriding setContentView but I recieve title to set title tooalber;
     * @param layoutResID
     * @param title
     */
    public void setContentView(int layoutResID, String title) {

        fullLayout = (RelativeLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        frameLayout = (FrameLayout) fullLayout.findViewById(R.id.content_frame);

        navigationView = (NavigationView) fullLayout.findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        getLayoutInflater().inflate(layoutResID, frameLayout, true);

        super.setContentView(fullLayout);

        this.title = title;
        drawNavigationDrawer();
    }


    private void drawNavigationDrawer() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(title);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }//onCreate

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }//onBackPressed

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        presenter = new MainPresenter(this);

        presenter.getUserType(new UserTypeCallback() {
            @Override
            public void onTrainerLogged(String user) {
                username = user;
                userType = getString(R.string.select_trainer);
                TextView textViewUsername = (TextView) findViewById(R.id.nav_user_name);
                TextView textViewUserType = (TextView) findViewById(R.id.nav_user_type);

                textViewUsername.setText(username);
                textViewUserType.setText(userType);

                getMenuInflater().inflate(R.menu.activity_main_drawer_trainer, navigationView.getMenu());
            }//onTrainerLogged

            @Override
            public void onTraineeLogged(String user) {
                username = user;
                userType = getString(R.string.select_trainee);

                TextView textViewUsername = (TextView) findViewById(R.id.nav_user_name);
                TextView textViewUserType = (TextView) findViewById(R.id.nav_user_type);

                textViewUsername.setText(username);
                textViewUserType.setText(userType);
                getMenuInflater().inflate(R.menu.activity_main_drawer_pupil, navigationView.getMenu());
            }//onTraineeLogged
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        final int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        presenter.getUserType(new UserTypeCallback() {
            @Override
            public void onTrainerLogged(String user) {
                switch (id) {
                    case R.id.nav_profile:
                        Intent intentProfile = new Intent(BaseActivity.this, ProfileActivity.class);
                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intentProfile.setAction(Constants.ACTION_TRAINER);
                        startActivity(intentProfile);
                        break;

                    case R.id.nav_home:
                        Intent intentMain = new Intent(BaseActivity.this, ContactsActivity.class);
                        intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intentMain);
                        break;

                    case R.id.nav_workout:
                        Intent intentWorkouts = new Intent(BaseActivity.this, WorkoutsActivity.class);
                        intentWorkouts.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intentWorkouts);
                        break;

                    case R.id.nav_send:
                        presenter.sendSuggestion(BaseActivity.this);
                        break;

                    case R.id.nav_share:
                        presenter.shareApp(BaseActivity.this);
                        break;
                }
            }//onTrainerLogged

            @Override
            public void onTraineeLogged(String user) {
                switch (id) {
                    case R.id.nav_home:
                        Intent intentMain = new Intent(BaseActivity.this, WorkoutsActivity.class);
                        intentMain.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intentMain);
                        break;

                    case R.id.nav_profile:
                        Intent intentProfile = new Intent(BaseActivity.this, ProfileActivity.class);
                        intentProfile.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intentProfile.setAction(Constants.ACTION_TRAINEE);
                        startActivity(intentProfile);
                        break;

                    case R.id.nav_send:
                        presenter.sendSuggestion(BaseActivity.this);
                        break;

                    case R.id.nav_share:
                        presenter.shareApp(BaseActivity.this);
                        break;

                }
            }//onTraineeLogged
        });

        return true;
    }
}//class
