/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.presenters;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.quique.u_tad.trainme.domain.interactors.UserInteractor;
import com.quique.u_tad.trainme.domain.interfaces.UserInteractorInterface;
import com.quique.u_tad.trainme.models.User;
import com.quique.u_tad.trainme.presentation.activities.FormActivity;
import com.quique.u_tad.trainme.presentation.callbacks.GetUserCallback;
import com.quique.u_tad.trainme.presentation.callbacks.SubmitFormCallback;
import com.quique.u_tad.trainme.presentation.fragments.FormFragment;
import com.quique.u_tad.trainme.presentation.interfaces.FormInterface;
import com.quique.u_tad.trainme.services.RegistrationIntentService;
import com.quique.u_tad.trainme.utils.Constants;
import com.quique.u_tad.trainme.utils.UserType;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo 
 * @date 22/4/16
 * @subject Programacion Plataformas Moviles
 */
public class FormPresenter implements FormInterface {

    UserInteractorInterface userInteractor;
    BroadcastReceiver tokenGeneratorBroadcastReceiver;
    boolean isGeneratedRegistered;


    public FormPresenter(Context context) {
        userInteractor = new UserInteractor();
        isGeneratedRegistered = false;
    }

    @Override
    public void createUser(final FormFragment fragment, final SubmitFormCallback callback) {
        final User user = getUserDataFromForm(fragment);

        if(user.isComplete(true)) {
            tokenGeneratorBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    SharedPreferences sharedPreferences =
                            PreferenceManager.getDefaultSharedPreferences(context);
                    String generateToken = sharedPreferences
                            .getString(Constants.PREF_TOKEN_GENERATED, Constants.NO_TOKEN);
                    if (!generateToken.equals(Constants.NO_TOKEN)) {

                        user.setToken(generateToken);
                        long res = userInteractor.addUser(user);

                        if (res == -1 || res == 0) {
                            callback.onError(-1);
                        } else {
                            callback.onSucces(user.getType());
                        }//if-else
//                    Toast.makeText(context, fragment.getString(R.string.gcm_send_message), Toast.LENGTH_LONG).show();
                    } else {
                        switch (generateToken) {
                            case Constants.NO_NETWORK:
                                Toast.makeText(context, fragment.getString(R.string.network_error_message), Toast.LENGTH_LONG).show();
                                break;

                            case Constants.NO_TOKEN:
                            default:
                                Toast.makeText(context, fragment.getString(R.string.token_error_message), Toast.LENGTH_LONG).show();
                                break;
                        }
                    }
                }
            };

            // Registering BroadcastReceiver
            registerReceiver((FormActivity) fragment.getActivity());

            if (checkPlayServices(fragment.getActivity())) {
                // Start IntentService to register this application with GCM.
                Intent intent = new Intent(fragment.getActivity(), RegistrationIntentService.class);
                fragment.getActivity().startService(intent);
            }//if
        }else{
            boolean[] errors = user.getErrors(true);
            String[] messageErrors = {fragment.getString(R.string.form_name),
                    fragment.getString(R.string.form_surname),
                    fragment.getString(R.string.form_mail),
                    fragment. getString(R.string.form_age),
                    fragment. getString(R.string.form_weight),
                    fragment. getString(R.string.form_height),
                    fragment.getString(R.string.form_token)};

            String message =fragment.getString(R.string.incomplete_form) + ":\r\n";
            for(int i = 0; i < errors.length; i++){
                if(!errors[i]){
                    message += "\t" + messageErrors[i] + "\r\n";
                }
            }

            // Registering BroadcastReceiver
            registerReceiver((FormActivity) fragment.getActivity());

            Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_LONG).show();
        }

    }//createContact

    @Override
    public void getUser(GetUserCallback callback) {
        User user = userInteractor.getUser();

        if (user != null) {
            callback.onSucces(user);
        } else {
            callback.onError();
        }
    }//getUser


    @Override
    public void editUser(final FormFragment fragment, SubmitFormCallback callback) {
        final User user = getUserDataFromForm(fragment);

        if(user.isComplete(true)) {

            long res = userInteractor.updateUser(user);

            if (res == 1) {
                callback.onSucces(user.getType());
            } else {
                callback.onError((int) res);
            }
        }else{
            boolean[] errors = user.getErrors(true);
            String[] messageErrors = {fragment.getString(R.string.form_name),
                    fragment.getString(R.string.form_surname),
                    fragment.getString(R.string.form_mail),
                    fragment. getString(R.string.form_age),
                    fragment. getString(R.string.form_weight),
                    fragment. getString(R.string.form_height),
                    fragment.getString(R.string.form_token)};

            String message =fragment.getString(R.string.incomplete_form) + ":\r\n";
            for(int i = 0; i < errors.length; i++){
                if(!errors[i]){
                    message += "\t" + messageErrors[i] + "\r\n";
                }
            }

            Toast.makeText(fragment.getActivity(), message, Toast.LENGTH_LONG).show();
            }
        }

        @NonNull
        private User getUserDataFromForm(FormFragment fragment) {
            final User user = new User();

            user.setName(fragment.getName());
            user.setSurname(fragment.getSurname());
            user.setEmail(fragment.getEmail());
            user.setType(fragment.getType());

            if (user.getType() == UserType.TRAINEE) {
                user.setAge(fragment.getAge());
                user.setHeight(fragment.getHeight());
                user.setWeight(fragment.getWeight());
            }//if


            return user;
        }

    private void registerReceiver(FormActivity activity) {
        if (!isGeneratedRegistered) {
            LocalBroadcastManager.getInstance(activity).registerReceiver(tokenGeneratorBroadcastReceiver,
                    new IntentFilter(Constants.PREF_TOKEN_GENERATED));
            isGeneratedRegistered = true;
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, Constants.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(Constants.LOG_TAG_GCM, "This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

}//class
