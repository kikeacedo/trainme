/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.models;

import com.quique.u_tad.trainme.utils.ValidateUser;

/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class Workout {

    private String title;
    private String description;
    private int id;

    public Workout() {
    }

    public Workout(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Workout(int id, String title, String description) {
        this.id = id;
        this.title = title;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Metodo para saber si los campos de un ejercicio estan completos o incompletos
     * @return true/false
     */
    public boolean isComplete() {
        boolean[] errors = getErrors();
        boolean result = true;

        for(int i = 0; i < errors.length && result; i++){
            if(!errors[i]){
                result = false;
            }
        }
        return result;
    }

    /**
     * Metodo para saber que campos del ejercicio estan erroneos
     * @return boolean[] true si OK, false si KO
     */
    public boolean[] getErrors() {
        boolean[] result = new boolean[2];

        for (int i = 0; i < result.length; i++) {
            result[i] = false;
        }

        if (!title.equals("")) {
            result[0] = true;
        }
        if (!description.equals("")) {
            result[1] = true;
        }
        return result;
    }
}
