/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.callbacks;

import com.quique.u_tad.trainme.utils.UserType;

/*
 * @author Quique Acedo 
 * @date 22/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface SubmitFormCallback {

    /**
     * Metodo ejecutado en caso de exito al enviar un formulario
     */
    void onSucces(UserType usertype);

    /**
     * Metodo ejecutado en caso de error al enviar un formulario
     * @param error
     */
    void onError(int error);
}//class
