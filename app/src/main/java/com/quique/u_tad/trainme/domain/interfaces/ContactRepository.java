/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interfaces;

import android.database.Cursor;

import com.quique.u_tad.trainme.models.Contact;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface ContactRepository {
    /**
     * Metodo para coger la lista de contactos almacenados en el movil
     * @return lista de contactos
     */
    ArrayList<Contact> getContacts();

    /**
     * Metodo para borrar un contacto
     * @param contact
     * @return numero de filas afectadas
     */
    int deleteContact(Contact contact);

    /**
     * Metodo para insertar un nuevo contacto
     * @param contact
     * @return respuesta de la base de datos
     */
    long insertContact(Contact contact);

    /**
     * Metodo para actualizar un contacto de la base de datos
     * @param contact
     * @return numero de filas afectadas
     */
    long updateContact(Contact contact);

    /**
     * Genera un Contact a partir de un cursor
     * @param cursor
     * @return contacto generado
     */
    Contact cursorToContact(Cursor cursor);

    /**
     * Metodo para coger un contacto a partir de su id
     * @param id
     * @return contacto
     */
    Contact getContact(int id);
}//class
