/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interfaces;

import com.quique.u_tad.trainme.models.Contact;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface ContactsInteractorInterface {
    /**
     * Metodo para coger los contactos del respositorio
     * @return List con los contactos
     */
    ArrayList<Contact> getContacts();

    /**
     * Metodo para eliminar un contacto del repositorio
     * @param contact
     * @return numero de filas afectadas
     */
    int removeContact(Contact contact);

    /**
     * Metodo para añadir un contacto al respositorio
     * @param contact
     * @return respuesta del repositorio
     */
    long addContact(Contact contact);

    /**
     * Metodo para coger un contacto dado su id
     * @param id
     * @return contacto
     */
    Contact getContact(int id);

    /**
     * Metodo para actualizar un contacto
     * @param contact
     * @return numero de filas afectadas
     */
    long updateContact(Contact contact);


}//class
