/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.quique.u_tad.trainme.models.Contact;
import com.quique.u_tad.trainme.presentation.presenters.ContactsPresenter;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import u_tad.quique.com.trainme.R;

/**
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 * <p>
 * used https://github.com/daimajia/AndroidSwipeLayout
 */
public class ContactSwipeAdapter extends RecyclerSwipeAdapter<ContactSwipeAdapter.ContactHolder> {

    private Activity activity;
    private ArrayList<Contact> contacts;
    private ContactsPresenter presenter;

    public ContactSwipeAdapter(Activity activity, ArrayList<Contact> objects, ContactsPresenter presenter) {
        this.activity = activity;
        this.contacts = objects;
        this.presenter = presenter;
    }

    @Override
    public ContactHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_swipe_card, parent, false);
        return new ContactHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactHolder viewHolder, final int position) {
        final Contact item = contacts.get(position);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCardSelected));
            }

            public void onClose(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCard));
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCardSelected));
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCard));
            }

        });

        if (item.getEmail().length() > 0) {
            viewHolder.buttonWorkout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.startSendWorkoutDialog(item.getToken());
                }
            });

            viewHolder.buttonEdit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.startFormItemDialog(item.getId());
                }
            });

            viewHolder.buttonDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    presenter.notifyItemRemoved(item);
                }
            });

        } else {
            viewHolder.buttonWorkout.setVisibility(View.INVISIBLE);
            viewHolder.swipeLayout.setVisibility(View.INVISIBLE);
        }

        viewHolder.name.setText(item.getName());
        viewHolder.email.setText(item.getEmail());
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public static class ContactHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        TextView name;
        TextView email;
        CircleImageView buttonWorkout;
        CircleImageView buttonEdit;
        CircleImageView buttonDelete;
        LinearLayout content;

        public ContactHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            name = (TextView) itemView.findViewById(R.id.textViewName);
            email = (TextView) itemView.findViewById(R.id.textViewEmail);
            buttonWorkout = (CircleImageView) itemView.findViewById(R.id.imageButtonWorkout);
            buttonEdit = (CircleImageView) itemView.findViewById(R.id.imageButtonEdit);
            buttonDelete = (CircleImageView) itemView.findViewById(R.id.imageButtonDelete);

            content = (LinearLayout) itemView.findViewById(R.id.content);
        }
    }

}
