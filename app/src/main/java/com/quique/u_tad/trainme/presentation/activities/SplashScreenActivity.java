/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.quique.u_tad.trainme.presentation.callbacks.LaunchActivityCallback;
import com.quique.u_tad.trainme.presentation.interfaces.SplashScreen;
import com.quique.u_tad.trainme.presentation.presenters.SplashScreenPresenter;
import com.quique.u_tad.trainme.utils.Constants;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo Dorado 
 * @date 20/4/16
 * @subject Programacion Plataformas Moviles
 */
public class SplashScreenActivity extends AppCompatActivity {

    // Set the duration of the splash screen
    private static final long DELAY = Constants.ANIMATION_TIME;
    private boolean isLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SplashScreen presenter = new SplashScreenPresenter(SplashScreenActivity.this.getApplicationContext());

                presenter.isLogged(new LaunchActivityCallback() {
                    @Override
                    public void launchActivity(Class<?> new_activity) {
                        Intent intent = new Intent(SplashScreenActivity.this, new_activity);
                        startActivity(intent);
                    }
                });

                finish();
            }
        }, DELAY);

    }//onCreate

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }//onPause


}//class
