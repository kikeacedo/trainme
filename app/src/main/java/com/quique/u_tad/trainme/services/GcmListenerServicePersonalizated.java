/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.quique.u_tad.trainme.App;
import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.presentation.activities.ContactsActivity;
import com.quique.u_tad.trainme.utils.Constants;

import u_tad.quique.com.trainme.R;


/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class GcmListenerServicePersonalizated extends GcmListenerService {
    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString(Constants.NOTIFICATION_MESSAGE);
        String workout_title = data.getString(Constants.NOTIFICATION_WORKOUT_TITLE, "NONE");
        String workout_description = data.getString(Constants.NOTIFICATION_WORKOUT_DESCRIPTION, "NONE");
        Log.d(Constants.LOG_TAG_GCM, "From: " + from);
        Log.d(Constants.LOG_TAG_GCM, "Message: " + message);

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */

        if (!workout_title.equals("NONE") && !workout_description.equals("NONE")) {
            App.getInstace().getWorkoutRepository().insertWorkout(new Workout(workout_title, workout_description));
        }

        if(message.equals(getString(R.string.notification_new_trainer))){
            SharedPreferences sharedPreferences =
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            sharedPreferences.edit().putString(Constants.NOTIFICATION_TRAINER,from).commit();
        }
        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */
        sendNotification(message);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */
    private void sendNotification(String message) {
        Intent intent = new Intent(this, ContactsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_logo)
                .setLargeIcon(bmp)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setPriority(Notification.PRIORITY_HIGH);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());
    }
}
