/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.interfaces;

import com.quique.u_tad.trainme.presentation.callbacks.GetContactCallback;
import com.quique.u_tad.trainme.presentation.callbacks.GetContactsCallback;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface ContactInterface {

    /**
     * Metodo para coger los contactos
     * @param callback
     */
    void getContacts(GetContactsCallback callback);

    /**
     * Metodo para coger un contacto
     * @param id
     * @param callback
     */
    void getContact(int id, GetContactCallback callback);
}//class
