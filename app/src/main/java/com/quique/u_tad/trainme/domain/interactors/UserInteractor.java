/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interactors;

import com.quique.u_tad.trainme.App;
import com.quique.u_tad.trainme.domain.interfaces.UserInteractorInterface;
import com.quique.u_tad.trainme.domain.interfaces.UserRepository;
import com.quique.u_tad.trainme.models.User;
import com.quique.u_tad.trainme.utils.UserType;

/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class UserInteractor implements UserInteractorInterface {

    private UserRepository userRepository;

    public UserInteractor() {
        userRepository = App.getInstace().getUserRepository();
    }

    @Override
    public boolean isLogged() {
        return userRepository.isSomeoneLogged();
    }//execute

    @Override
    public User getUser() {
        return userRepository.getUserLogged();
    }

    @Override
    public UserType getUserType(){return userRepository.getUserLogged().getType();}

    @Override
    public long addUser(User user) {
        return userRepository.insertUser(user);
    }

    @Override
    public long updateUser(User user) {
        return userRepository.updateUser(user);
    }
}//class
