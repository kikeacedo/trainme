/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.quique.u_tad.trainme.domain.interfaces.WorkoutRepository;
import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.utils.Constants;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 8/5/16
 * @subject Programacion Plataformas Moviles
 */
public class WorkoutSqlRepository extends SQLiteOpenHelper implements WorkoutRepository {

    private final String TABLE_WORKOUT = "workout";
    private final String COL_TITLE = "title";
    private final String COL_DESCRIPTION = "description";
    private final String COL_ID = "id";

    private static final String DDBB_NAME = "com.quique.trainme.workout.db";
    private static final int DDBB_VERSION = 1;


    public WorkoutSqlRepository(Context context) {
        super(context, DDBB_NAME, null, DDBB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_WORKOUT + "(" +
                COL_ID + " integer primary key autoincrement," +
                COL_TITLE + " string," +
                COL_DESCRIPTION + " string)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public ArrayList<Workout> getWorkouts() {

        ArrayList<Workout> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        if (db != null) {
            Cursor cursor = db.query(TABLE_WORKOUT,
                    null, null, null, null, null, null);

//            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                list.add(cursorToWorkout(cursor));
            }//while
        }//if
        return list;
    }

    @Override
    public int deleteWorkout(Workout workout) {
        int id = workout.getId();
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(TABLE_WORKOUT, COL_ID
                + " = '" + id + "'", null);
    }

    @Override
    public long insertWorkout(Workout workout) {
        Long res = 0L;
        ContentValues cv = new ContentValues();
        cv.put(COL_TITLE, workout.getTitle());
        cv.put(COL_DESCRIPTION, workout.getDescription());

        try {
            SQLiteDatabase db = getWritableDatabase();
            res = db.insertWithOnConflict(TABLE_WORKOUT, null, cv,
                    SQLiteDatabase.CONFLICT_IGNORE);
            Log.d(Constants.LOG_TAG_BBDD, "INSERTED Workout " + workout.getTitle());

        } catch (SQLiteException e) {
            Log.e(Constants.LOG_TAG_BBDD, "unable to access writable database to INSERT Workout");
            Log.e(Constants.LOG_TAG_BBDD, e.toString());
        }//try-catch

        return res;
    }

    @Override
    public long updateWorkout(Workout workout) {
        long res = 0;
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, workout.getId());
        cv.put(COL_TITLE, workout.getTitle());
        cv.put(COL_DESCRIPTION, workout.getDescription());

        try {
            SQLiteDatabase db = getWritableDatabase();
            res = db.updateWithOnConflict(TABLE_WORKOUT, cv, COL_ID + " = '" + workout.getId() + "'", null,
                    SQLiteDatabase.CONFLICT_REPLACE);
            Log.d(Constants.LOG_TAG_BBDD, "UPDATED Workout " + workout.getTitle());

        } catch (SQLiteException e) {
            Log.e(Constants.LOG_TAG_BBDD, "unable to access writable database to UPDATE Workout");
            Log.e(Constants.LOG_TAG_BBDD, e.toString());
        }//try-catch

        return res;
    }

    @Override
    public Workout cursorToWorkout(Cursor cursor) {
        Workout workout = new Workout();
        workout.setTitle(cursor.getString(cursor.getColumnIndex(COL_TITLE)));
        workout.setDescription(cursor.getString(cursor.getColumnIndex(COL_DESCRIPTION)));
        workout.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));

        return workout;
    }

    @Override
    public Workout getWorkout(int id) {
        Workout workout = null;

        if (id >= 0) {
            String where = COL_ID + " = '" + id + "'";

            Cursor cursor = getReadableDatabase().query(TABLE_WORKOUT,
                    null, where, null, null, null, null);

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                workout = cursorToWorkout(cursor);
            }
        }

        return workout;
    }

}//class
