/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.utils;

/*
 * @author Quique Acedo 
 * @date 20/4/16
 * @subject Programacion Plataformas Moviles
 */
public class Constants {

    public static final String PREF_NAME = "preferences";
    public static final int ANIMATION_TIME = 3000;

    public static final String ACTION_TRAINER = "action.trainer";
    public static final String ACTION_TRAINEE = "action.trainee";

    public static final String LOG_TAG_BBDD = "Trainme.Database";
    public static final String LOG_TAG_GCM = "Trainme.GCM";

    public static final String USER_TYPE_TRAINER = "trainer";
    public static final String USER_TYPE_TRAINEE = "trainee";

    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PREF_TOKEN_GENERATED = "tokenGenerated";
    public static final String PREF_USER_TYPE = "userType";
    public static final String NO_TOKEN = "noTokenGenerated";
    public static final String NO_NETWORK = "noNetworkAvailable";


    public static final String ACTION_ADD = "action.add";
    public static final String ACTION_EDIT = "action.edit";

    public static final String API_KEY = "AIzaSyAMs39IYn3spi8V0vi1P3r9_PGv1KDlsJk";

    public static final String HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HEADER_AUTHORIZATION = "Authorization";

    public static final String CONTENT_TYPE_FORM_ENCODED = "application/x-www-form-urlencoded";
    public static final String CONTENT_TYPE_JSON = "application/json";

    public static final String NOTIFICATION_MESSAGE = "gcm.message";
    public static final String NOTIFICATION_TOKEN_ID = "gcm.token_id";
    public static final String NOTIFICATION_WORKOUT_TITLE = "gcm.workout_title";
    public static final String NOTIFICATION_WORKOUT_DESCRIPTION = "gcm.workout_description";
    public static final String NOTIFICATION_TRAINER ="gcm.trainer";

}//class
