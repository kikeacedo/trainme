/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.models;

import com.quique.u_tad.trainme.utils.UserType;
import com.quique.u_tad.trainme.utils.ValidateUser;

/*
 * @author Quique Acedo 
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class User {

    int id;
    UserType type;
    String email;
    String name;
    String surname;
    int age;
    double weight;
    double height;
    String token;

    public User() {

    }

    public User(int id, String name, String surname, String email, UserType type) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    /**
     * Metodo para saber si los campos de un usuario estan completos o incompletos
     * @return true/false
     */
    public boolean isComplete(boolean creating){
        boolean[] errors = getErrors(creating);
        boolean result = true;

        for(int i = 0; i < errors.length && result; i++){
            if(!errors[i]){
                result = false;
            }
        }
        return result;
    }


    /**
     * Metodo para saber que campos del usuario estan erroneos
     * @return boolean[] true si OK, false si KO
     */
    public boolean[] getErrors(boolean creating){
        boolean[]result;
        if(type == UserType.TRAINER){
            result = new boolean[3];
            if(!name.equals("") && ValidateUser.isValidName(name)){
                result[0] = true;
            }
            if(!surname.equals("") && ValidateUser.isValidName(surname)){
                result[1] = true;
            }
            if(!email.equals("") && ValidateUser.isValidEmail(email)) {
                result[2] = true;
            }
        }else {
            result = new boolean[6];


            if (!name.equals("") && ValidateUser.isValidName(name)) {
                result[0] = true;
            }
            if (!surname.equals("") && ValidateUser.isValidName(surname)) {
                result[1] = true;
            }
            if (!email.equals("") && ValidateUser.isValidEmail(email)) {
                result[2] = true;
            }

            if (age > 10 && age < 100) {
                result[3] = true;
            }
            if (weight > 30 && weight < 200) {
                result[4] = true;
            }
            if (height > 70 && height < 250) {
                result[5] = true;
            }

//            if (!creating) {
//                if (token != null && !token.equals("")) {
//                    result[6] = true;
//                }
//            }
        }
        return result;
    }
}//class
