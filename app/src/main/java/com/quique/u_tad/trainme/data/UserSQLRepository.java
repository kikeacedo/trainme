/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.data;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.quique.u_tad.trainme.domain.interfaces.UserRepository;
import com.quique.u_tad.trainme.models.User;
import com.quique.u_tad.trainme.utils.Constants;
import com.quique.u_tad.trainme.utils.Translator;
import com.quique.u_tad.trainme.utils.UserType;

/*
 * @author Quique Acedo 
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class UserSqlRepository extends SQLiteOpenHelper implements UserRepository {

    private final String TABLE_USER = "user";
    private final String COL_NAME = "name";
    private final String COL_SURNAME = "surname";
    private final String COL_MAIL = "mail";
    private final String COL_AGE = "age";
    private final String COL_WEIGHT = "weight";
    private final String COL_HEIGHT = "height";
    private final String COL_TYPE = "usertype";
    private final String COL_TOKEN = "token";

    private static final String DDBB_NAME = "com.quique.trainme.user.db";
    private static final int DDBB_VERSION = 1;


    public UserSqlRepository(Context context) {
        super(context, DDBB_NAME, null, DDBB_VERSION);
    }//constructor


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_USER + "(" +
                "id integer primary key," +
                COL_NAME + " string," +
                COL_SURNAME + " string," +
                COL_MAIL + " string unique," +
                COL_AGE + " int," +
                COL_WEIGHT + " double," +
                COL_HEIGHT + " double," +
                COL_TYPE + " string," +
                COL_TOKEN + " string)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public boolean isSomeoneLogged() {
        return getUserLogged() != null;
    }

    @Override
    public User getUserLogged() {
        User user = null;

        SQLiteDatabase db = getReadableDatabase();

        if (db != null) {
            Cursor cursor = db.query(TABLE_USER,
                    null, null, null, null, null, null);

            if (cursor.moveToFirst()) {
                user = new User();
                user.setName(cursor.getString(cursor.getColumnIndex(COL_NAME)));
                user.setSurname(cursor.getString(cursor.getColumnIndex(COL_SURNAME)));
                user.setEmail(cursor.getString(cursor.getColumnIndex(COL_MAIL)));
                user.setType(Translator.getUserType(cursor.getString(cursor.getColumnIndex(COL_TYPE))));
                user.setToken(cursor.getString(cursor.getColumnIndex(COL_TOKEN)));

                if (user.getType() == UserType.TRAINEE) {
                    user.setAge(cursor.getInt(cursor.getColumnIndex(COL_AGE)));
                    user.setHeight(cursor.getDouble(cursor.getColumnIndex(COL_HEIGHT)));
                    user.setWeight(cursor.getDouble(cursor.getColumnIndex(COL_WEIGHT)));
                }//if
            }//if
        }//if
        return user;
    }

    @Override
    public long insertUser(User user) {
        Long res = 0L;
        ContentValues cv = new ContentValues();
        cv.put(COL_NAME, user.getName());
        cv.put(COL_SURNAME, user.getSurname());
        cv.put(COL_MAIL, user.getEmail());
        cv.put(COL_AGE, user.getAge());
        cv.put(COL_WEIGHT, user.getWeight());
        cv.put(COL_HEIGHT, user.getHeight());
        cv.put(COL_TYPE, user.getType().toString());
        cv.put(COL_TOKEN, user.getToken());

        try {
            SQLiteDatabase db = getWritableDatabase();
            res = db.insertWithOnConflict(TABLE_USER, null, cv,
                    SQLiteDatabase.CONFLICT_IGNORE);
            Log.d(Constants.LOG_TAG_BBDD, "INSERTED User " + user.getEmail() + " - " + user.getToken());

        } catch (SQLiteException se) {
            Log.e(Constants.LOG_TAG_BBDD, "unable to access writable database to INSERT User");
            Log.e(Constants.LOG_TAG_BBDD, se.toString());
        }//try-catch

        return res;
    }//inserUser()

    @Override
    public long updateUser(User user) {
        long res = 0;
        ContentValues cv = new ContentValues();
        cv.put(COL_NAME, user.getName());
        cv.put(COL_SURNAME, user.getSurname());
        cv.put(COL_MAIL, user.getEmail());
        cv.put(COL_AGE, user.getAge());
        cv.put(COL_WEIGHT, user.getWeight());
        cv.put(COL_HEIGHT, user.getHeight());
        cv.put(COL_TYPE, user.getType().toString());

        try {
            SQLiteDatabase db = getWritableDatabase();
            res = db.updateWithOnConflict(TABLE_USER, cv, null, null,
                    SQLiteDatabase.CONFLICT_REPLACE);
        } catch (SQLiteException se) {
            Log.e(Constants.LOG_TAG_BBDD, "unable to access writable database to UPDATE User");
        }//try-catch
        return res;
    }//inserUser()

}//class
