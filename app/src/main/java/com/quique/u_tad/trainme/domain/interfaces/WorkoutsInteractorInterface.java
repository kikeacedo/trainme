/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interfaces;

import com.quique.u_tad.trainme.models.Workout;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 8/5/16
 * @subject Programacion Plataformas Moviles
 */
public interface WorkoutsInteractorInterface {

    /**
     * Metodo para eliminar un ejercicio del repositorio
     * @param workout
     * @return numero de filas afectadas
     */
    int removeWorkout(Workout workout);

    /**
     * Metodo para coger los ejercicios del repositorio
     * @return lista con los ejercicios
     */
    ArrayList<Workout> getWorkouts();

    /**
     * Metodo para añadir un ejercicio al repositorio
     * @param workout
     * @return respuesta del repositorio
     */
    long addWorkout(Workout workout);

    /**
     * Metodo para coger un ejercicio dado su id
     * @param id
     * @return ejercicio
     */
    Workout getWorkout(int id);

    /**
     * Metodo para actualizar un ejercicio
     * @param workout
     * @return numero de filas afectadas
     */
    long updateWorkout(Workout workout);

}//class
