/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.activities;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.daimajia.swipe.util.Attributes;
import com.quique.u_tad.trainme.models.Contact;
import com.quique.u_tad.trainme.presentation.adapters.ContactSwipeAdapter;
import com.quique.u_tad.trainme.presentation.callbacks.GetContactsCallback;
import com.quique.u_tad.trainme.presentation.presenters.ContactsPresenter;
import com.quique.u_tad.trainme.utils.DividerItemDecoration;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo Dorado
 * @date 20/4/16
 * @subject Programacion Plataformas Moviles
 */
public class ContactsActivity extends BaseActivity {

    public FloatingActionButton addButton;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private ContactsPresenter presenter;
    private RecyclerView.Adapter adapter;
    public Animation rotate_forward, rotate_backward;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts, getString(R.string.nav_home));


        addButton = (FloatingActionButton) findViewById(R.id.addContactButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startFormItemDialog(-1);
                presenter.animateFab();
            }//onClick
        });

        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);


        recyclerView = (RecyclerView) findViewById(R.id.contact_listview);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
        recyclerView.setItemAnimator(new FadeInLeftAnimator());

        presenter = new ContactsPresenter(this);
        presenter.getContacts(new GetContactsCallback() {
            @Override
            public void onSucces(ArrayList<Contact> contacts) {
                adapter = new ContactSwipeAdapter(ContactsActivity.this, contacts, presenter);
                ((ContactSwipeAdapter) adapter).setMode(Attributes.Mode.Single);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onError() {
                adapter = new ContactSwipeAdapter(ContactsActivity.this, new ArrayList<Contact>(), presenter);
                ((ContactSwipeAdapter) adapter).setMode(Attributes.Mode.Single);
                recyclerView.setAdapter(adapter);
            }
        });

    }//onCreate

    /**
     * Metodo que recarga la lista de contactos
     * @param contacts
     */
    public void reloadAdapter(ArrayList<Contact> contacts) {
        adapter = new ContactSwipeAdapter(ContactsActivity.this, contacts, presenter);
        ((ContactSwipeAdapter) adapter).setMode(Attributes.Mode.Single);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Metodo que anima al FAB de la activity
     * @param anim
     */
    public void animateFab(Animation anim) {
        addButton.startAnimation(anim);
    }


    @Override
    public void onResume() {
        super.onResume();
        presenter.reloadFab();
    }
}//class
