/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.quique.u_tad.trainme.models.Contact;
import com.quique.u_tad.trainme.presentation.activities.ContactsActivity;
import com.quique.u_tad.trainme.presentation.callbacks.GetContactCallback;
import com.quique.u_tad.trainme.presentation.presenters.ContactsPresenter;
import com.quique.u_tad.trainme.utils.Constants;
import com.quique.u_tad.trainme.utils.UserType;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class FormContactDialogFragment extends DialogFragment {

    public FormContactDialogFragment() {
        this.id = -1;
    }

    @SuppressLint("ValidFragment")
    public FormContactDialogFragment(int id) {
        this.id = id;
        presenter = new ContactsPresenter((ContactsActivity) getActivity());
    }

    private GetContactCallback callback;
    private AlertDialog.Builder builder;
    private EditText inputTextName, inputTextSurname, inputTextMail,
            inputTextAge, inputTextHeight, inputTextWeight, inputTextToken;

    private int id;
    private ContactsPresenter presenter;

    /**
     * Setea el callback
     * @param callback
     */
    public void setCallback(GetContactCallback callback) {
        this.callback = callback;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(getActivity(),R.style.AppCompatAlertDialogStyleForm);

        builder.setTitle(R.string.dialog_title_contact);


        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_form_contact, null);
        builder.setView(view);

        inputTextName = (EditText) view.findViewById(R.id.inputTextName);
        inputTextSurname = (EditText) view.findViewById(R.id.inputTextSurname);
        inputTextMail = (EditText) view.findViewById(R.id.inputTextMail);
        inputTextAge = (EditText) view.findViewById(R.id.inputTextAge);
        inputTextWeight = (EditText) view.findViewById(R.id.inputTextWeight);
        inputTextHeight = (EditText) view.findViewById(R.id.inputTextHeight);
        inputTextToken = (EditText) view.findViewById(R.id.inputTextToken);

        builder.setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                callback.onError(false);
            }
        });

        final AlertDialog alert = builder.create();

        if(id >= 0) {
            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button button = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    button.setText(R.string.dialog_edit);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Contact contact = getData();

                            if(contact.isComplete(false)) {
                                callback.onSucces(contact, Constants.ACTION_EDIT);
                                dismiss();
                            }else{
                                boolean[] errors = contact.getErrors(false);
                                String[] messageErros = {getString(R.string.form_name),
                                        getString(R.string.form_surname),
                                        getString(R.string.form_mail),
                                        getString(R.string.form_token),
                                        getString(R.string.form_age),
                                        getString(R.string.form_weight),
                                        getString(R.string.form_height)};

                                String message = getString(R.string.incomplete_form) + ":\r\n";
                                for(int i = 0; i < errors.length; i++){
                                    if(!errors[i]){
                                        message += "\t" + messageErros[i] + "\r\n";
                                    }
                                }
                                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            });
            presenter.getContact(id, new GetContactCallback() {
                @Override
                public void onSucces(Contact contact, String action) {
                    builder.setTitle(R.string.dialog_edit_contact);
                    inputTextName.setText(contact.getName());
                    inputTextSurname.setText(contact.getSurname());
                    inputTextMail.setText(contact.getEmail());
                    inputTextAge.setText(String.valueOf(contact.getAge()));
                    inputTextHeight.setText(String.valueOf(contact.getHeight()));
                    inputTextWeight.setText(String.valueOf(contact.getWeight()));
                    inputTextToken.setText(contact.getToken());
                }

                @Override
                public void onError(boolean notify) { }
            });
        }else {
            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button button = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Contact contact = getData();

                            if (contact.isComplete(false)) {
                                callback.onSucces(contact, Constants.ACTION_ADD);
                                dismiss();
                            } else {
                                boolean[] errors = contact.getErrors(false);
                                String[] messageErros = {getString(R.string.form_name),
                                        getString(R.string.form_surname),
                                        getString(R.string.form_mail),
                                        getString(R.string.form_age),
                                        getString(R.string.form_weight),
                                        getString(R.string.form_height),
                                        getString(R.string.form_token)};

                                String message = getString(R.string.incomplete_form) + ":\r\n";
                                for (int i = 0; i < errors.length; i++) {
                                    if (!errors[i]) {
                                        message += "\t" + messageErros[i] + "\r\n";
                                    }
                                }
                                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                }
            });
        }

        // Create the AlertDialog object and return it
        return alert;
    }

    private Contact getData() {
        Contact contact = new Contact();
        contact.setId(id);
        contact.setName(inputTextName.getText().toString());
        contact.setSurname(inputTextSurname.getText().toString());
        contact.setEmail(inputTextMail.getText().toString());
        if (!inputTextAge.getText().toString().equals("")) {
            contact.setAge(Integer.valueOf(inputTextAge.getText().toString()));
        } else {
            contact.setAge(0);
        }
        if (!inputTextHeight.getText().toString().equals("")) {
            contact.setHeight(Double.parseDouble(inputTextHeight.getText().toString()));
        } else {
            contact.setHeight(0);
        }
        if (!inputTextWeight.getText().toString().equals("")) {
            contact.setWeight(Double.parseDouble(inputTextWeight.getText().toString()));
        } else {
            contact.setWeight(0);
        }
        contact.setToken(inputTextToken.getText().toString());
        contact.setType(UserType.TRAINEE);

        return contact;
    }
}
