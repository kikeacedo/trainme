/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.presentation.callbacks.GetWorkoutsCallback;
import com.quique.u_tad.trainme.presentation.callbacks.SendWorkoutCallback;
import com.quique.u_tad.trainme.presentation.presenters.WorkoutsPresenter;

import java.util.ArrayList;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class SendWorkoutDialogFragment extends DialogFragment {

    private SendWorkoutCallback callback;
    private AlertDialog.Builder builder;
    private String token;
    boolean[] selected;
    Object[] workouts;
    private WorkoutsPresenter presenter;

    public SendWorkoutDialogFragment() {
    }

    @SuppressLint("ValidFragment")
    public SendWorkoutDialogFragment(String token) {
        this.token = token;
        presenter = new WorkoutsPresenter();
    }


    public void setCallback(SendWorkoutCallback callback) {
        this.callback = callback;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);

        builder.setTitle(R.string.dialog_title_send_workout);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_send_workout, null);
        builder.setView(view);

        builder.setPositiveButton(R.string.dialog_send, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                ArrayList<Workout> workouts = getSelectedWorkouts();

                if (workouts.size() > 0) {
                    callback.onSucces(token, workouts);
                } else {
                    callback.onError();
                }
            }
        });

        builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                callback.onError();
            }
        });

        presenter.getWorkouts(new GetWorkoutsCallback() {
            @Override
            public void onSucces(ArrayList<Workout> result) {

                workouts = result.toArray();

                final CharSequence[] items = new CharSequence[workouts.length - 1];
                selected = new boolean[workouts.length - 1];

                for (int i = 0; i < workouts.length - 1; i++) {
                    Workout aux = (Workout) workouts[i];
                    items[i] = aux.getTitle();
                    selected[i] = false;
                }

                builder.setMultiChoiceItems(items, selected, new DialogInterface.OnMultiChoiceClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                        selected[which] = isChecked;
                    }
                });
            }

            @Override
            public void onError() {

            }
        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    private ArrayList<Workout> getSelectedWorkouts() {
        ArrayList<Workout> result = new ArrayList<>();

        for (int i = 0; i < workouts.length - 1; i++) {
            if (selected[i]) {
                result.add((Workout) workouts[i]);
            }
        }

        return result;
    }

}
