/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.presenters;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;

import com.quique.u_tad.trainme.domain.interactors.UserInteractor;
import com.quique.u_tad.trainme.domain.interfaces.UserInteractorInterface;
import com.quique.u_tad.trainme.models.User;
import com.quique.u_tad.trainme.presentation.callbacks.UserTypeCallback;
import com.quique.u_tad.trainme.presentation.interfaces.SocialInterface;
import com.quique.u_tad.trainme.presentation.interfaces.UserTypeInterface;
import com.quique.u_tad.trainme.utils.Constants;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class MainPresenter implements SocialInterface, UserTypeInterface {

    private UserInteractorInterface userInteractor;
    private Activity activity;

    public MainPresenter(Activity activity) {
        userInteractor = new UserInteractor();
        this.activity = activity;
    }

    @Override
    public void getUserType(UserTypeCallback callback) {
        User user = userInteractor.getUser();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(activity);

        if (user != null) {
            switch (user.getType()) {
                case TRAINEE:
                    callback.onTraineeLogged(user.getName());
                    preferences.edit().putString(Constants.PREF_USER_TYPE, Constants.USER_TYPE_TRAINEE).commit();
                    break;
                case TRAINER:
                    callback.onTrainerLogged(user.getName());
                    preferences.edit().putString(Constants.PREF_USER_TYPE, Constants.USER_TYPE_TRAINER).commit();
                    break;
            }//switch
        }//if
    }//getUser

    @Override
    public void sendSuggestion(Activity activity) {
        String email = "kike.acedo@gmail.com";
        String subject = activity.getString(R.string.contact_subject);
        String body = "";

        User user = userInteractor.getUser();

        if (user != null) {
            body += activity.getString(R.string.mail_username) + " " + user.getName() + " " + user.getSurname() + "<br/>";
            body += activity.getString(R.string.mail_mail) + " " + user.getEmail() + "<br/>";
            body += "-------------" + "<br/>";
            body += activity.getString(R.string.mail_contact)+ "<br/><br/>";
        }

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, Html.fromHtml(body));
        try {
            activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.send_mail)));
        } catch (ActivityNotFoundException exception) {
            Log.e("Error: ", exception.toString());
        }
    }

    @Override
    public void shareApp(Activity activity) {
        String subject = activity.getString(R.string.share_subject);
        String body = activity.getString(R.string.share_body);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        try {
            activity.startActivity(Intent.createChooser(intent, activity.getString(R.string.send_mail)));
        } catch (ActivityNotFoundException exception) {
            Log.e("Error: ", exception.toString());
        }
    }
}//class
