/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.callbacks;

import com.quique.u_tad.trainme.models.Contact;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface GetContactsCallback {

    /**
     * Metodo llamado en caso de exito en la busqueda del contactos
     * @param contacts
     */
    void onSucces(ArrayList<Contact> contacts);

    /**
     * Metodo llamado en caso de error en la busqueda del contactos
     */
    void onError();
}//class
