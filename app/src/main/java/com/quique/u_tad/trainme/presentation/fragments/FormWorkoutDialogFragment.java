/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.presentation.activities.WorkoutsActivity;
import com.quique.u_tad.trainme.presentation.callbacks.GetWorkoutCallback;
import com.quique.u_tad.trainme.presentation.presenters.WorkoutsPresenter;
import com.quique.u_tad.trainme.utils.Constants;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo
 * @date 8/5/16
 * @subject Programacion Plataformas Moviles
 */
public class FormWorkoutDialogFragment extends DialogFragment {

    private GetWorkoutCallback callback;
    private AlertDialog.Builder builder;
    private EditText inputTextTitle, inputTextDescription;
    private int id;
    private WorkoutsPresenter presenter;

    public FormWorkoutDialogFragment() {
        this.id = -1;
    }

    @SuppressLint("ValidFragment")
    public FormWorkoutDialogFragment(int id) {
        this.id = id;
        presenter = new WorkoutsPresenter((WorkoutsActivity) getActivity());
    }


    public void setCallback(GetWorkoutCallback callback) {
        this.callback = callback;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        builder = new AlertDialog.Builder(getActivity(),R.style.AppCompatAlertDialogStyleForm);

        builder.setTitle(R.string.dialog_title_workout);

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_form_workout, null);
        builder.setView(view);

        inputTextTitle = (EditText) view.findViewById(R.id.inputTextTitle);
        inputTextDescription = (EditText) view.findViewById(R.id.inputTextDescription);

        builder.setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                callback.onError(false);
            }
        });

        final AlertDialog alert = builder.create();

        if(id >= 0) {
            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button button = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    button.setText(R.string.dialog_edit);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Workout workout = getData();

                            if(workout.isComplete()) {
                                callback.onSucces(workout, Constants.ACTION_EDIT);
                                dismiss();
                            }else{
                                boolean[] errors = workout.getErrors();
                                String[] messageErros = {getString(R.string.form_title),
                                        getString(R.string.form_description)};

                                String message = getString(R.string.incomplete_form) + ":\r\n";
                                for(int i = 0; i < errors.length; i++){
                                    if(!errors[i]){
                                        message += "\t" + messageErros[i] + "\r\n";

                                    }
                                }
                                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            });

            presenter.getWorkout(id, new GetWorkoutCallback() {
                @Override
                public void onSucces(Workout workout, String action ) {
                    builder.setTitle(R.string.dialog_edit_workout);
                    inputTextTitle.setText(workout.getTitle());
                    inputTextDescription.setText(workout.getDescription());
                }

                @Override
                public void onError(boolean notify) { }
            });
        }else{
            alert.setOnShowListener(new DialogInterface.OnShowListener() {
                @Override
                public void onShow(DialogInterface dialog) {
                    Button button = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Workout workout = getData();

                            if(workout.isComplete()) {
                                callback.onSucces(workout, Constants.ACTION_ADD);
                                dismiss();
                            }else{
                                boolean[] errors = workout.getErrors();
                                String[] messageErros = {getString(R.string.form_title),
                                        getString(R.string.form_description)};

                                String message = getString(R.string.incomplete_form) + ":\r\n";
                                for(int i = 0; i < errors.length; i++){
                                    if(!errors[i]){
                                        message += "\t" + messageErros[i] + "\r\n";

                                    }
                                }
                                Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            });
        }


        // Create the AlertDialog object and return it
        return alert;
    }

    private Workout getData() {
        Workout workout = new Workout();
        workout.setId(id);
        workout.setTitle(inputTextTitle.getText().toString());
        workout.setDescription(inputTextDescription.getText().toString());

        return workout;
    }
}
