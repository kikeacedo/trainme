/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.quique.u_tad.trainme.models.User;
import com.quique.u_tad.trainme.presentation.activities.ContactsActivity;
import com.quique.u_tad.trainme.presentation.activities.ProfileActivity;
import com.quique.u_tad.trainme.presentation.activities.SplashScreenActivity;
import com.quique.u_tad.trainme.presentation.activities.WorkoutsActivity;
import com.quique.u_tad.trainme.presentation.callbacks.GetUserCallback;
import com.quique.u_tad.trainme.presentation.callbacks.SubmitFormCallback;
import com.quique.u_tad.trainme.presentation.presenters.FormPresenter;
import com.quique.u_tad.trainme.utils.Constants;
import com.quique.u_tad.trainme.utils.Translator;
import com.quique.u_tad.trainme.utils.UserType;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo
 * @date 8/5/16
 * @subject Programacion Plataformas Moviles
 */
public class FormFragment extends Fragment {

    private EditText inputTextName, inputTextSurname, inputTextMail,
            inputTextAge, inputTextHeight, inputTextWeight, inputTextToken;

    private TextView title;

    private Button submitButton;
    private String status;
    private FormPresenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_form_contact, container, false);

        presenter = new FormPresenter(getActivity().getApplicationContext());

        inputTextName = (EditText) view.findViewById(R.id.inputTextName);
        inputTextSurname = (EditText) view.findViewById(R.id.inputTextSurname);
        inputTextMail = (EditText) view.findViewById(R.id.inputTextMail);
        inputTextAge = (EditText) view.findViewById(R.id.inputTextAge);
        inputTextWeight = (EditText) view.findViewById(R.id.inputTextWeight);
        inputTextHeight = (EditText) view.findViewById(R.id.inputTextHeight);
        inputTextToken = (EditText) view.findViewById(R.id.inputTextToken);
        submitButton = (Button) view.findViewById(R.id.submitButton);

        submitButton.setVisibility(View.VISIBLE);
//        inputTextToken.setVisibility(View.GONE);

        Intent intent = getActivity().getIntent();
        status = intent.getAction();

        if (status.equals(Constants.ACTION_TRAINER)) {
            hidePupilFields();
        }//switch

        getUserData();

        return view;
    }

    private void getUserData() {
        presenter.getUser(new GetUserCallback() {
            @Override
            public void onError() {
                setupSubmitButtons();
            }

            @Override
            public void onSucces(User user) {
                setupSaveButtons();
                inputTextName.setText(user.getName());
                inputTextSurname.setText(user.getSurname());
                inputTextMail.setText(user.getEmail());
                status = user.getType().toString();
                inputTextToken.setText(user.getToken());

                Log.d("TOKEN_ID --> ", user.getToken());

                if (user.getType() == UserType.TRAINEE) {
                    inputTextAge.setText(String.valueOf(user.getAge()));
                    inputTextWeight.setText(String.valueOf(user.getWeight()));
                    inputTextHeight.setText(String.valueOf(user.getHeight()));
                } else {
                    hidePupilFields();
                }
            }
        });
    }

    private void hidePupilFields() {
        inputTextAge.setVisibility(View.GONE);
        inputTextWeight.setVisibility(View.GONE);
        inputTextHeight.setVisibility(View.GONE);
    }

    private void setupSubmitButtons() {
        inputTextToken.setVisibility(View.GONE);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.createUser(FormFragment.this, new SubmitFormCallback() {
                    @Override
                    public void onSucces(UserType usertype) {
                        Toast.makeText(getActivity(), R.string.form_submit_succes, Toast.LENGTH_LONG).show();
                        Intent intent;
                        switch (usertype){
                            case TRAINEE:
                               intent = new Intent(getActivity(), WorkoutsActivity.class);
                                break;
                            case TRAINER:
                                intent = new Intent(getActivity(), ContactsActivity.class);
                                break;
                            default:
                                intent = new Intent(getActivity(), SplashScreenActivity.class);
                        }
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        getActivity().startActivity(intent);
                    }//onSucces

                    @Override
                    public void onError(int error) {
                        Toast.makeText(getActivity(), R.string.form_submit_error + "[" + error + "]", Toast.LENGTH_LONG).show();

                    }//onError
                });
            }//onClick
        });
    }//setupButtons

    private void setupSaveButtons() {
        submitButton.setText(getString(R.string.form_save));
        inputTextToken.setVisibility(View.GONE);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                presenter.editUser(FormFragment.this, new SubmitFormCallback() {
                    @Override
                    public void onSucces(UserType usertype) {
                        Toast.makeText(getActivity(), R.string.form_save_succes, Toast.LENGTH_LONG).show();
                        ProfileActivity profileActivity = (ProfileActivity) getActivity();
                        profileActivity.reloadMenu();
                    }

                    @Override
                    public void onError(int error) {
                        Toast.makeText(getActivity(), R.string.form_save_error + "[" + error + "]", Toast.LENGTH_LONG).show();

                    }
                });
            }//onClick
        });
    }//setupButtons

    public String getName() {
        return inputTextName.getText().toString();
    }

    public String getSurname() {
        return inputTextSurname.getText().toString();
    }

    public String getEmail() {
        return inputTextMail.getText().toString();
    }

    public int getAge(){
        if(!inputTextAge.getText().toString().equals("")) {
            return Integer.valueOf(inputTextAge.getText().toString());
        }else{
            return 0;
        }
    }

    public double getHeight(){
        if(!inputTextHeight.getText().toString().equals("")) {
            return Double.parseDouble(inputTextHeight.getText().toString());
        }else{
            return 0;
        }
    }
    public double getWeight() {
        if (!inputTextWeight.getText().toString().equals("")) {
            return Double.parseDouble(inputTextWeight.getText().toString());
        } else {
            return 0;
        }
    }

    public UserType getType() {
        return Translator.getUserType(status);
    }
}
