/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interfaces;

import com.quique.u_tad.trainme.models.User;

/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface UserRepository {

    /**
     * Metodo para saber si hay algun usuario logead
     * @return true/false
     */
    boolean isSomeoneLogged();

    /**
     * Metodo que devuelve el usuario logeado
     * @return usuario
     */
    User getUserLogged();

    /**
     * Metodo para insertar un usuario nuevo
     * @param user
     * @return respuesta del servidor
     */
    long insertUser(User user);

    /**
     * Metodo para actualizar el usuario
     * @param user
     * @return numero de filas afectadas
     */
    long updateUser(User user);

}
