/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.presenters;

import android.content.Intent;
import android.widget.Toast;

import com.quique.u_tad.trainme.domain.interactors.ContactInteractor;
import com.quique.u_tad.trainme.domain.interfaces.ContactsInteractorInterface;
import com.quique.u_tad.trainme.models.Contact;
import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.presentation.activities.ContactsActivity;
import com.quique.u_tad.trainme.presentation.callbacks.GetContactCallback;
import com.quique.u_tad.trainme.presentation.callbacks.GetContactsCallback;
import com.quique.u_tad.trainme.presentation.callbacks.SendWorkoutCallback;
import com.quique.u_tad.trainme.presentation.fragments.FormContactDialogFragment;
import com.quique.u_tad.trainme.presentation.fragments.SendWorkoutDialogFragment;
import com.quique.u_tad.trainme.presentation.interfaces.AnimationFloatingButtonInterface;
import com.quique.u_tad.trainme.presentation.interfaces.ContactInterface;
import com.quique.u_tad.trainme.presentation.interfaces.ListInterface;
import com.quique.u_tad.trainme.presentation.interfaces.NewItemDialog;
import com.quique.u_tad.trainme.presentation.interfaces.SendWorkoutInterface;
import com.quique.u_tad.trainme.services.GcmSenderService;
import com.quique.u_tad.trainme.utils.Constants;

import java.util.ArrayList;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public class ContactsPresenter implements ContactInterface, NewItemDialog,
        AnimationFloatingButtonInterface, ListInterface, SendWorkoutInterface {

    private ContactsInteractorInterface contactInteractor;
    private ContactsActivity activity;
    private boolean isFabOpen = false;

    public ContactsPresenter(ContactsActivity activity) {
        contactInteractor = new ContactInteractor();
        this.activity = activity;
    }


    @Override
    public void getContacts(GetContactsCallback callback) {
        ArrayList<Contact> list = contactInteractor.getContacts();
        list.add(new Contact("", ""));

        if (list.size() > 0) {
            callback.onSucces(list);
        } else {
            callback.onError();
        }//if-else
    }//getContacts

    private void getContactsFromDB() {
        ArrayList<Contact> list = contactInteractor.getContacts();

        list.add(new Contact("", ""));

        activity.reloadAdapter(list);
    }

    @Override
    public void notifyItemInserted(Object object) {
        final Contact contact = (Contact) object;

        contactInteractor.addContact(contact);
        Intent intent = new Intent(activity, GcmSenderService.class);
        intent.putExtra(Constants.NOTIFICATION_MESSAGE, activity.getString(R.string.notification_new_trainer));
        intent.putExtra(Constants.NOTIFICATION_TOKEN_ID, contact.getToken());
        activity.startService(intent);
    }

    @Override
    public void notifyItemRemoved(final Object object) {
        final Contact contact = (Contact) object;
        contactInteractor.removeContact(contact);

        getContacts(new GetContactsCallback() {
            @Override
            public void onSucces(ArrayList<Contact> contacts) {
                if (contacts != null) {
                    activity.reloadAdapter(contacts);
                    Toast.makeText(activity, activity.getText(R.string.card_delete) + " " + contact.getEmail() + "!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onError() {
                Toast.makeText(activity, activity.getText(R.string.card_delete_error) + " " + contact.getEmail() + "!", Toast.LENGTH_SHORT).show();
            }
        });

    }


    @Override
    public void getContact(int id, GetContactCallback callback) {

        Contact contact = contactInteractor.getContact(id);

        if (contact != null) {
            callback.onSucces(contact, Constants.ACTION_EDIT);
        } else {
            callback.onError(false);
        }

    }

    @Override
    public void animateFab() {
        if (isFabOpen) {
            activity.animateFab(activity.rotate_backward);
            isFabOpen = false;
        } else {
            activity.addButton.startAnimation(activity.rotate_forward);
            isFabOpen = true;
        }
    }

    @Override
    public void reloadFab() {
        if (isFabOpen) {
            activity.animateFab(activity.rotate_backward);
            isFabOpen = false;
        }
    }

    @Override
    public void startFormItemDialog(int id) {
        FormContactDialogFragment formDialog;

        if (id >= 0) {
            formDialog = new FormContactDialogFragment(id);
        } else {
            formDialog = new FormContactDialogFragment();
        }

        formDialog.setCallback(new GetContactCallback() {
            @Override
            public void onSucces(Contact contact, String action) {

                switch (action) {
                    case Constants.ACTION_ADD:
                        notifyItemInserted(contact);
                        break;

                    case Constants.ACTION_EDIT:
                        contactInteractor.updateContact(contact);
                        break;
                }

                getContactsFromDB();
                reloadFab();
            }

            @Override
            public void onError(boolean notify) {
                if(notify) {
                    Toast.makeText(activity, activity.getText(R.string.dialog_create_contact_error) + "!", Toast.LENGTH_SHORT).show();
                }
                reloadFab();
            }
        });

        formDialog.show(activity.getFragmentManager(), "FormFragment");
    }


    @Override
    public void startSendWorkoutDialog(String token) {
        SendWorkoutDialogFragment sendDialog;

        if (token != null && token.length() > 0) {
            sendDialog = new SendWorkoutDialogFragment(token);
        } else {
            sendDialog = new SendWorkoutDialogFragment();
        }

        sendDialog.setCallback(new SendWorkoutCallback() {
            @Override
            public void onSucces(String token, ArrayList<Workout> workouts) {

                for (Workout workout : workouts) {
                    Intent intent = new Intent(activity, GcmSenderService.class);
                    intent.putExtra(Constants.NOTIFICATION_MESSAGE, activity.getString(R.string.notification_new_workout));
                    intent.putExtra(Constants.NOTIFICATION_TOKEN_ID, token);
                    intent.putExtra(Constants.NOTIFICATION_WORKOUT_TITLE, workout.getTitle());
                    intent.putExtra(Constants.NOTIFICATION_WORKOUT_DESCRIPTION, workout.getDescription());

                    activity.startService(intent);
                }
            }

            @Override
            public void onError() {
//                Toast.makeText(activity, activity.getText(R.string.dialog_create_contact_error) + "!", Toast.LENGTH_SHORT).show();
//                reloadFab();
            }
        });

        sendDialog.show(activity.getFragmentManager(), "SendFragment");
    }
}//class
