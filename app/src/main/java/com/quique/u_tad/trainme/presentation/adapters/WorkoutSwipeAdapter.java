/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.adapters;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.presentation.callbacks.UserTypeCallback;
import com.quique.u_tad.trainme.presentation.presenters.WorkoutsPresenter;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import u_tad.quique.com.trainme.R;

/**
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 * used https://github.com/daimajia/AndroidSwipeLayout
 */
public class WorkoutSwipeAdapter extends RecyclerSwipeAdapter<WorkoutSwipeAdapter.WorkoutHolder> {

    private Activity activity;
    private ArrayList<Workout> workouts;
    private WorkoutsPresenter presenter;

    public WorkoutSwipeAdapter(Activity activity, ArrayList<Workout> objects, WorkoutsPresenter presenter) {
        this.activity = activity;
        this.workouts = objects;
        this.presenter = presenter;
    }

    @Override
    public WorkoutHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.workout_swipe_card, parent, false);
        return new WorkoutHolder(view);
    }

    @Override
    public void onBindViewHolder(final WorkoutHolder viewHolder, final int position) {
        final Workout item = workouts.get(position);
        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        viewHolder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCardSelected));
            }

            public void onClose(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCard));
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCardSelected));
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                viewHolder.content.setBackground(activity.getDrawable(R.color.backgroundCard));
            }

        });

        presenter.getUserType(new UserTypeCallback() {
            @Override
            public void onTrainerLogged(String username) {

                viewHolder.firstButton.setImageResource(R.drawable.ic_mode_edit_rounded);
                viewHolder.firstButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        presenter.startFormItemDialog(item.getId());
                    }
                });


                viewHolder.secondButton.setImageResource(R.drawable.ic_delete_rounded);
                viewHolder.secondButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.notifyItemRemoved(item);
                    }
                });

            }

            @Override
            public void onTraineeLogged(String username) {
                viewHolder.firstButton.setImageResource(R.drawable.ic_discard_rounded);
                viewHolder.firstButton.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        presenter.notifyItemRemoved(item);
                        Toast.makeText(activity,activity.getString(R.string.workout_discard), Toast.LENGTH_SHORT).show();
                    }
                });


                viewHolder.secondButton.setImageResource(R.drawable.ic_done_rounded);
                viewHolder.secondButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        presenter.notifyItemRemoved(item);
                        Toast.makeText(view.getContext(), activity.getString(R.string.workout_done), Toast.LENGTH_SHORT).show();

                    }
                });

            }
        });

        viewHolder.title.setText(item.getTitle());
        viewHolder.description.setText(item.getDescription());
    }

    @Override
    public int getItemCount() {
        return workouts.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public static class WorkoutHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        TextView title;
        TextView description;
        CircleImageView firstButton;
        CircleImageView secondButton;
        LinearLayout content;

        public WorkoutHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            title = (TextView) itemView.findViewById(R.id.textViewTitle);
            description = (TextView) itemView.findViewById(R.id.textViewDescription);
            firstButton = (CircleImageView) itemView.findViewById(R.id.imageButtonDiscard);
            secondButton = (CircleImageView) itemView.findViewById(R.id.imageButtonDone);
            content = (LinearLayout) itemView.findViewById(R.id.content);
        }
    }

}
