/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.utils;

/*
 * @author Quique Acedo 
 * @date 22/4/16
 * @subject Programacion Plataformas Moviles
 */
public class Translator {


    /**
     * Transform String type to UserType type
     * @param type tipo de usuario que se quiere coge
     * @return UserType
     */
    public static UserType getUserType(String type) {
        switch (type) {
            case Constants.USER_TYPE_TRAINEE:
            case Constants.ACTION_TRAINEE:
            case "TRAINEE":
                return UserType.TRAINEE;

            case Constants.USER_TYPE_TRAINER:
            case Constants.ACTION_TRAINER:
            case "TRAINER":
                return UserType.TRAINER;

            default:
                return UserType.UNKNOWN;
        }//switch
    }//getUserType
}//class
