/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interfaces;

import android.database.Cursor;

import com.quique.u_tad.trainme.models.Workout;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 26/4/16
 * @subject Programacion Plataformas Moviles
 */
public interface WorkoutRepository {

    /**
     * Metodo para coger los ejercicios almacenados en el movil
     * @return ejercicios
     */
    ArrayList<Workout> getWorkouts();

    /**
     * Metodo para insertar un nuevo ejercicio
     * @param workout
     * @return respuesta de la base de datos
     */
    int deleteWorkout(Workout workout);

    /**
     * Metodo para insertar un nuevo ejercicio
     * @param workout
     * @return respuesta de la base de datos
     */
    long insertWorkout(Workout workout);

    /**
     * Metodo para actualizar un ejercicio
     * @param workout
     * @return numero de filas afectadas
     */
    long updateWorkout(Workout workout);

    /**
     * Genera un Workout a partir de un cursor
     * @param cursor
     * @return ejercicio generado
     */
    Workout cursorToWorkout(Cursor cursor);

    /**
     * Metodo para coger un ejercicio a partir de su id
     * @param id
     * @return ejercicio
     */
    Workout getWorkout(int id);
}//class
