/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.interfaces;

/*
 * @author Quique Acedo 
 * @date 8/5/16
 * @subject Programacion Plataformas Moviles
 */
public interface ListInterface {
    /**
     * Metodo para eliminar un item de una lista
     * @param object
     */
    void notifyItemRemoved(Object object);

    /**
     * Metodo para insertar un elemento de una lista
     * @param object
     */
    void notifyItemInserted(Object object);
}//class
