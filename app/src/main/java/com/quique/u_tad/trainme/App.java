/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.quique.u_tad.trainme.data.ContactSqlRepository;
import com.quique.u_tad.trainme.data.UserSqlRepository;
import com.quique.u_tad.trainme.data.WorkoutSqlRepository;
import com.quique.u_tad.trainme.domain.interfaces.ContactRepository;
import com.quique.u_tad.trainme.domain.interfaces.UserRepository;
import com.quique.u_tad.trainme.domain.interfaces.WorkoutRepository;

import u_tad.quique.com.trainme.BuildConfig;

/*
 * @author Quique Acedo 
 * @date 22/4/16
 * @subject Programacion Plataformas Moviles
 */
public class App extends Application {

    public static GoogleAnalytics analytics;
    public static Tracker tracker;


    private UserRepository userRepository;
    private ContactRepository contactRepository;
    private WorkoutRepository workoutRepository;

    private static App instace;

    public static App getInstace() {
        if (instace == null) {
            instace = new App();
        }
        return instace;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // https://github.com/googleanalytics/hello-world-android-app
        analytics = GoogleAnalytics.getInstance(this);
        tracker = analytics.newTracker("UA-60263914-1");
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        // alternative automatic screen measurement xml config
        // https://developers.google.com/analytics/devguides/collection/android/v4/screens
        tracker.enableAutoActivityTracking(true);

        analytics.setDryRun(BuildConfig.DEVELOP);

        instace = this;

        userRepository = new UserSqlRepository(this);
        contactRepository = new ContactSqlRepository(this);
        workoutRepository = new WorkoutSqlRepository(this);
    }

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public ContactRepository getContactRepository() {
        return contactRepository;
    }

    public WorkoutRepository getWorkoutRepository() {
        return workoutRepository;
    }
}//class
