/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.services;

import android.os.AsyncTask;

import com.quique.u_tad.trainme.models.Message;
import com.quique.u_tad.trainme.presentation.callbacks.ToastCallback;
import com.quique.u_tad.trainme.utils.Constants;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class GcmSender {


    private ToastCallback callback;

    public GcmSender(ToastCallback callback) {
        this.callback = callback;
    }

    /**
     * Send downstream GCM
     * @param registrationID
     * @param data
     */
    public void doGcmSend(String registrationID, HashMap<String, String> data) {

        final Message.Builder messageBuilder = new Message.Builder();

        messageBuilder.delayWhileIdle(false);
        messageBuilder.dryRun(false);


        Iterator<Map.Entry<String, String>> iterator = data.entrySet().iterator();

        while (iterator.hasNext()) {
            Map.Entry<String, String> aux = iterator.next();
            messageBuilder.addData(aux.getKey(), aux.getValue());
        }

        final String apiKey = Constants.API_KEY;
        final String registrationId = registrationID;

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                GcmServerSideSender sender = new GcmServerSideSender(apiKey);
                try {
                    sender.sendHttpJsonDownstreamMessage(registrationId,
                            messageBuilder.build());

                } catch (final IOException e) {
                    return e.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                if (result != null) {
                    callback.onError();
                } else {
                    callback.onSucces();
                }
            }
        }.execute();

    }

}
