/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interactors;

import com.quique.u_tad.trainme.App;
import com.quique.u_tad.trainme.domain.interfaces.WorkoutRepository;
import com.quique.u_tad.trainme.domain.interfaces.WorkoutsInteractorInterface;
import com.quique.u_tad.trainme.models.Workout;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 8/5/16
 * @subject Programacion Plataformas Moviles
 */
public class WorkoutsInteractor implements WorkoutsInteractorInterface {

    private WorkoutRepository workoutRepository;

    public WorkoutsInteractor() {
        workoutRepository = App.getInstace().getWorkoutRepository();
    }


    @Override
    public int removeWorkout(Workout workout) {
        return workoutRepository.deleteWorkout(workout);
    }

    @Override
    public ArrayList<Workout> getWorkouts() {
        return workoutRepository.getWorkouts();
    }

    @Override
    public long addWorkout(Workout workout) {
        return workoutRepository.insertWorkout(workout);
    }

    @Override
    public Workout getWorkout(int id) {
        return workoutRepository.getWorkout(id);
    }

    @Override
    public long updateWorkout(Workout workout) {
        return workoutRepository.updateWorkout(workout);
    }
}//class
