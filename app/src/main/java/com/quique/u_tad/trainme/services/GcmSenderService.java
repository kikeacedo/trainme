/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.services;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.quique.u_tad.trainme.presentation.callbacks.ToastCallback;
import com.quique.u_tad.trainme.utils.Constants;

import java.util.HashMap;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class GcmSenderService extends IntentService {

    GcmSender sender;

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public GcmSenderService() {
        super("GcmSenderService");
        sender = new GcmSender(new ToastCallback() {
            @Override
            public void onSucces() {
                Toast.makeText(getBaseContext(), getString(R.string.gcm_send_message),
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError() {
                Toast.makeText(getBaseContext(), getString(R.string.gcm_send_message_error),
                        Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.i(Constants.LOG_TAG_GCM, "Entra");
        String message = intent.getStringExtra(Constants.NOTIFICATION_MESSAGE);
        String token_id = intent.getStringExtra(Constants.NOTIFICATION_TOKEN_ID);
        String workout_title = intent.getStringExtra(Constants.NOTIFICATION_WORKOUT_TITLE);
        String workout_description = intent.getStringExtra(Constants.NOTIFICATION_WORKOUT_DESCRIPTION);


        sendMessage(token_id, message, workout_title, workout_description);

    }

    public void sendMessage(String token_id, String message, String title, String description) {
        HashMap<String, String> data = new HashMap<>();
        data.put(Constants.NOTIFICATION_MESSAGE, message);
        data.put(Constants.NOTIFICATION_WORKOUT_TITLE, title);
        data.put(Constants.NOTIFICATION_WORKOUT_DESCRIPTION, description);
        sender.doGcmSend(token_id, data);
    }
}
