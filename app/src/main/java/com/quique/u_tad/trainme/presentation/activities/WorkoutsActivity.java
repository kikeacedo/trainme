/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.activities;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.daimajia.swipe.util.Attributes;
import com.quique.u_tad.trainme.models.Workout;
import com.quique.u_tad.trainme.presentation.adapters.WorkoutSwipeAdapter;
import com.quique.u_tad.trainme.presentation.callbacks.GetWorkoutsCallback;
import com.quique.u_tad.trainme.presentation.callbacks.UserTypeCallback;
import com.quique.u_tad.trainme.presentation.presenters.WorkoutsPresenter;
import com.quique.u_tad.trainme.utils.Constants;
import com.quique.u_tad.trainme.utils.DividerItemDecoration;

import java.util.ArrayList;

import jp.wasabeef.recyclerview.animators.FadeInLeftAnimator;
import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo 
 * @date 29/4/16
 * @subject Programacion Plataformas Moviles
 */
public class WorkoutsActivity extends BaseActivity {

    public FloatingActionButton floatingActionButton;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private WorkoutsPresenter presenter;
    private RecyclerView.Adapter adapter;
    public Animation rotate_forward, rotate_backward;


    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts, getString(R.string.nav_workout));

        toolbar.setTitle(getString(R.string.nav_workout));

        floatingActionButton = (FloatingActionButton) findViewById(R.id.sendUserToken);

        rotate_forward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_backward);

        recyclerView = (RecyclerView) findViewById(R.id.workouts_listview);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addItemDecoration(new DividerItemDecoration(getResources().getDrawable(R.drawable.divider)));
        recyclerView.setItemAnimator(new FadeInLeftAnimator());

        presenter = new WorkoutsPresenter(this);

        presenter.getUserType(new UserTypeCallback() {
            @Override
            public void onTrainerLogged(String username) {
                floatingActionButton.setImageResource(R.drawable.ic_add_white_24dp);
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.startFormItemDialog(-1);
                        presenter.animateFab();
                    }//onClick
                });


            }

            @Override
            public void onTraineeLogged(String username) {
                floatingActionButton.setImageResource(R.drawable.ic_send_white_24dp);
                floatingActionButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        presenter.sendTokenByEmail();
                        presenter.animateFab();
                    }//onClick
                });
            }
        });

        presenter.getWorkouts(new GetWorkoutsCallback() {
            @Override
            public void onSucces(ArrayList<Workout> workouts) {
                adapter = new WorkoutSwipeAdapter(WorkoutsActivity.this, workouts, presenter);
                ((WorkoutSwipeAdapter) adapter).setMode(Attributes.Mode.Single);
                recyclerView.setAdapter(adapter);
                SharedPreferences sharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

                String trainer = sharedPreferences.getString(Constants.NOTIFICATION_TRAINER, "NONE");

                if(!trainer.equals("NONE")){
                    floatingActionButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError() {
                adapter = new WorkoutSwipeAdapter(WorkoutsActivity.this, new ArrayList<Workout>(), presenter);
                ((WorkoutSwipeAdapter) adapter).setMode(Attributes.Mode.Single);
                recyclerView.setAdapter(adapter);
            }
        });

    }//onCreate

    /**
     * Metodo que recarga la lista de workouts
     * @param workouts
     */
    public void reloadAdapter(ArrayList<Workout> workouts) {
        adapter = new WorkoutSwipeAdapter(WorkoutsActivity.this, workouts, presenter);
        ((WorkoutSwipeAdapter) adapter).setMode(Attributes.Mode.Single);
        recyclerView.setAdapter(adapter);
    }

    /**
     * Metodo que anima al FAB de la activity
     * @param anim
     */
    public void animateFab(Animation anim) {
        floatingActionButton.startAnimation(anim);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.reloadFab();
    }
}//class
