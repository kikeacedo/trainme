/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.presentation.activities;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.quique.u_tad.trainme.utils.Constants;

import u_tad.quique.com.trainme.R;

/*
 * @author Quique Acedo Dorado 
 * @date 20/4/16
 * @subject Programacion Plataformas Moviles
 */
public class LoginActivity extends AppCompatActivity {

    private ImageView buttonTrainer;
    private ImageView buttonPupil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Set portrait orientation
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        // Hide title bar
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        buttonTrainer = (ImageView) findViewById(R.id.buttonTrainer);
        buttonPupil = (ImageView) findViewById(R.id.buttonTrainee);


        buttonTrainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, FormActivity.class);
                intent.setAction(Constants.ACTION_TRAINER);
                LoginActivity.this.startActivity(intent);
            }
        });

        buttonPupil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, FormActivity.class);
                intent.setAction(Constants.ACTION_TRAINEE);
                LoginActivity.this.startActivity(intent);
            }
        });

    }//onCreate
}//class
