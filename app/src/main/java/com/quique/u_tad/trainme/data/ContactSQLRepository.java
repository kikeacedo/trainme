/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.quique.u_tad.trainme.domain.interfaces.ContactRepository;
import com.quique.u_tad.trainme.models.Contact;
import com.quique.u_tad.trainme.utils.Constants;
import com.quique.u_tad.trainme.utils.Translator;
import com.quique.u_tad.trainme.utils.UserType;

import java.util.ArrayList;

/*
 * @author Quique Acedo 
 * @date 21/4/16
 * @subject Programacion Plataformas Moviles
 */
public class ContactSqlRepository extends SQLiteOpenHelper implements ContactRepository {

    private final String TABLE_CONTACT = "contact";
    private final String COL_ID = "id";
    private final String COL_NAME = "name";
    private final String COL_SURNAME = "surname";
    private final String COL_MAIL = "mail";
    private final String COL_AGE = "age";
    private final String COL_WEIGHT = "weight";
    private final String COL_HEIGHT = "height";
    private final String COL_TYPE = "usertype";
    private final String COL_TOKEN = "token";

    private static final String DDBB_NAME = "com.quique.trainme.contact.db";
    private static final int DDBB_VERSION = 1;


    public ContactSqlRepository(Context context) {
        super(context, DDBB_NAME, null, DDBB_VERSION);
    }//constructor


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_CONTACT + "(" +
                "id integer primary key autoincrement," +
                COL_NAME + " string," +
                COL_SURNAME + " string," +
                COL_MAIL + " string unique," +
                COL_AGE + " int," +
                COL_WEIGHT + " double," +
                COL_HEIGHT + " double," +
                COL_TYPE + " string," +
                COL_TOKEN + " string)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    @Override
    public ArrayList<Contact> getContacts() {
        ArrayList<Contact> list = new ArrayList<>();
        SQLiteDatabase db = getReadableDatabase();

        if (db != null) {
            Cursor cursor = db.query(TABLE_CONTACT,
                    null, null, null, null, null, null);

//            cursor.moveToFirst();
            while (cursor.moveToNext()) {
                list.add(cursorToContact(cursor));
            }//while
        }//if
        return list;
    }

    @Override
    public int deleteContact(Contact contact) {
        String email = contact.getEmail();
        SQLiteDatabase database = getWritableDatabase();
        return database.delete(TABLE_CONTACT, COL_MAIL
                + " = '" + email + "'", null);
    }

    @Override
    public long insertContact(Contact contact) {
        Long res = 0L;
        ContentValues cv = new ContentValues();
        cv.put(COL_NAME, contact.getName());
        cv.put(COL_SURNAME, contact.getSurname());
        cv.put(COL_MAIL, contact.getEmail());
        cv.put(COL_AGE, contact.getAge());
        cv.put(COL_WEIGHT, contact.getWeight());
        cv.put(COL_HEIGHT, contact.getHeight());
        cv.put(COL_TYPE, contact.getType().toString());
        cv.put(COL_TOKEN, contact.getToken());

        try {
            SQLiteDatabase db = getWritableDatabase();
            res = db.insertWithOnConflict(TABLE_CONTACT, null, cv,
                    SQLiteDatabase.CONFLICT_IGNORE);
            Log.d(Constants.LOG_TAG_BBDD, "INSERTED Contact " + contact.getEmail() + " - " + contact.getToken());

        } catch (SQLiteException se) {
            Log.e(Constants.LOG_TAG_BBDD, "unable to access writable database to INSERT Contact");
            Log.e(Constants.LOG_TAG_BBDD, se.toString());
        }//try-catch

        return res;
    }

    @Override
    public long updateContact(Contact contact) {
        long res = 0;
        ContentValues cv = new ContentValues();
        cv.put(COL_ID, contact.getId());
        cv.put(COL_NAME, contact.getName());
        cv.put(COL_SURNAME, contact.getSurname());
        cv.put(COL_MAIL, contact.getEmail());
        cv.put(COL_AGE, contact.getAge());
        cv.put(COL_WEIGHT, contact.getWeight());
        cv.put(COL_HEIGHT, contact.getHeight());
        cv.put(COL_TYPE, contact.getType().toString());
        cv.put(COL_TOKEN, contact.getToken());

        try {
            SQLiteDatabase db = getWritableDatabase();
            res = db.updateWithOnConflict(TABLE_CONTACT, cv, COL_ID + " = '" + contact.getId() + "'", null,
                    SQLiteDatabase.CONFLICT_REPLACE);
            Log.e(Constants.LOG_TAG_BBDD, "UPDATE CONTACT " + contact.getEmail());

        } catch (SQLiteException se) {
            Log.e(Constants.LOG_TAG_BBDD, "unable to access writable database to UPDATE CONTACT");
        }//try-catch
        return res;
    }

    @Override
    public Contact cursorToContact(Cursor cursor) {
        Contact contact = new Contact();
        contact.setId(cursor.getInt(cursor.getColumnIndex(COL_ID)));
        contact.setName(cursor.getString(cursor.getColumnIndex(COL_NAME)));
        contact.setSurname(cursor.getString(cursor.getColumnIndex(COL_SURNAME)));
        contact.setEmail(cursor.getString(cursor.getColumnIndex(COL_MAIL)));
        contact.setType(Translator.getUserType(cursor.getString(cursor.getColumnIndex(COL_TYPE))));
        contact.setToken(cursor.getString(cursor.getColumnIndex(COL_TOKEN)));

        if (contact.getType() == UserType.TRAINEE) {
            contact.setAge(cursor.getInt(cursor.getColumnIndex(COL_AGE)));
            contact.setHeight(cursor.getDouble(cursor.getColumnIndex(COL_HEIGHT)));
            contact.setWeight(cursor.getDouble(cursor.getColumnIndex(COL_WEIGHT)));
        }//if
        return contact;
    }//cursorToContact

    @Override
    public Contact getContact(int id) {
        Contact contact = null;

        if (id >= 0) {
            String where = COL_ID + " = '" + id + "'";

            Cursor cursor = getReadableDatabase().query(TABLE_CONTACT,
                    null, where, null, null, null, null);

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                contact = cursorToContact(cursor);
            }
        }

        return contact;
    }
}//class
