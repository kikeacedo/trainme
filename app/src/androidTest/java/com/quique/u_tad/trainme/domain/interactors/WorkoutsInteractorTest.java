/*
 * Copyright (c) 2016. Quique Acedo
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE­2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.quique.u_tad.trainme.domain.interactors;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by quique on 09/05/16.
 */
public class WorkoutsInteractorTest {

    @Test
    public void testRemoveWorkout() throws Exception {
        assertEquals(1,1);

    }

    @Test
    public void testGetWorkouts() throws Exception {
        assertEquals(1,1);

    }

    @Test
    public void testAddWorkout() throws Exception {
        assertEquals(1,1);

    }

    @Test
    public void testGetWorkout() throws Exception {
        assertEquals(1,1);

    }

    @Test
    public void testUpdateWorkout() throws Exception {
        assertEquals(1,1);

    }
}