# Trainme #

Puedes descargarlo en: [Google Play](https://play.google.com/store/apps/details?id=com.quique.u_tad.trainme&hl=es)

TrainMe es una aplicación para la gestion de entrenamientos tanto para el entrenador personal como para el deportista. Permite una comunicacion diaria y una gestión y control de todos los ejercicios que se deben hacer.

Para el entrenador:

* Guarda todos tus ejercicios con explicaciones y compartelos con otros entrenadores!
* Ten un listado de todos tus deportistas.
* Comparte con tus alumnos los ejercicios que deben de hacer a diario a traves de notificaciones.

Para el deportista:

* Ten un listado de los ejericicios que debes hacer para completar tu objetivo!